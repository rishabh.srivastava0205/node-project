import axios from "axios";

const axiosInstance = axios.create({
    baseURL: 'http://localhost:5000/api'
});

axiosInstance.interceptors.request.use((config) => {
    const jwtToken = localStorage.getItem('ACCESS_TOKEN');
    config.headers.Authorization = `Bearer ${jwtToken}`;
    return config;
});

// response interceptor
axiosInstance.interceptors.response.use((response) => {
    return response;
}, (error) => {
    const {response} = error;
    if (response.status === 401) {
        localStorage.removeItem('ACCESS_TOKEN');
    }
    throw error;
})


export default axiosInstance;
import {createBrowserRouter} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./style-sheet/style.css";
import AppLayout from "./Components/Layouts/AppLayout";
import HeadLayout from "./Components/Layouts/HeadLayout";
import Login from "./Components/Auths/Login";
import Dashboard from "./Components/Dashboard";
import UsersComponent from "./Components/Users/UsersComponent";
import NotFound from "./Components/NotFound";
import AdminHeadLayout from "./Admin/Layouts/AdminHeadLayout"
import AdminDashboard from "./Admin/AdminDashboard";
import AdminLogin from "./Admin/Auths/AdminLogin";


const Router = createBrowserRouter([
    {
        path: '/',
        children: [
            {
                path: '/',
                element: <HeadLayout />,
                children: [
                    {
                        path: '/',
                        element: <Dashboard />
                    },
                    {
                        path: '/users',
                        element: <UsersComponent />
                    }
                ]
            },
            {
                path: '/',
                element: <AppLayout />,
                children: [
                    {
                        path: '/login',
                        element: <Login />
                    }
                ]
            },
            {
                path: "*",
                element: <NotFound/>
            }
        ]
    },
    {
        path: '/admin',
        children: [
            {
                path: '/admin',
                element: <AdminHeadLayout />,
                children: [
                    {
                        path: '/',
                        element: <AdminDashboard />
                    },
                ]
            },
            {
                path: '/admin',
                element: <AdminAppLayout />,
                children: [
                    {
                        path: '/login',
                        element: <AdminLogin />
                    },
                ]
            }

        ]
    }
    
]);

export default Router;
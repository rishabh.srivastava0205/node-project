import {createContext, useContext, useState} from "react";

// Has to provide default value for authcomplision.
const StateContext = createContext({
    authUser: null,
    token: null,
    setAuthUser: () => {},
    manageToken: () => {}
});

// Need to create Context Provider
export const ContextProvider = ({children}) => {
    const [authUser, setAuthUser] = useState({});
    const [token, setToken] = useState(localStorage.getItem('ACCESS_TOKEN'));

    const manageToken = (token) => {
        setToken(token);
        if(token) {
            localStorage.setItem('ACCESS_TOKEN', token); }
        else if(!token) {
            localStorage.removeItem('ACCESS_TOKEN'); }
    }

    return (
        <StateContext.Provider value={{
            authUser,
            token,
            setAuthUser,
            manageToken
        }}>
            {children}
        </StateContext.Provider>
    );
}

export const useStateContext = () => useContext(StateContext)


